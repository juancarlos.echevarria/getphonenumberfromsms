package com.example.sendmessage;
import android.Manifest;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.os.Bundle;

import android.util.Log;
import android.widget.TextView;

import android.content.IntentFilter;
import android.widget.Button;
import android.widget.EditText;
import android.view.View;
import android.widget.Toast;
import android.content.pm.PackageManager;


public class MainActivity extends AppCompatActivity {
    private TextView sender;
    private TextView content;
    private IntentFilter receiveFilter;
    private MessageReceiver messageReceiver;
    private EditText to;
    private EditText msgInput;
    private Button send;
    public static final String LOG_TAG = "myLogs";
    private static final String ACTION_SMS_SEND = "lab.sodino.sms.send";
    private static final String ACTION_SMS_DELIVERY = "lab.sodino.sms.delivery";
    private static final String ACTION_SMS_RECEIVER = "android.provider.Telephony.SMS_RECEIVED";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sender = (TextView)  findViewById(R.id.sender);
        content = (TextView)  findViewById(R.id.content);
        MessageReceiver smsReceiver= new MessageReceiver();
        IntentFilter receiverFilter = new IntentFilter(ACTION_SMS_RECEIVER);

        registerReceiver(smsReceiver, receiverFilter);
        send = (Button) findViewById(R.id.send);


        String[] permissions = {Manifest.permission.RECEIVE_SMS};
        requestPermissions(permissions, 200);


        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED)
                {
                    Log.d(LOG_TAG, "Ingreso al primer if");
                    MessageSent();

                }
                else
                {
                    ActivityCompat.requestPermissions(MainActivity.this,new String[]{Manifest.permission.RECEIVE_SMS, Manifest.permission.READ_PHONE_STATE, Manifest.permission.SEND_SMS},100);
                }
            }
        });
    }



    private void MessageSent() {
        Log.d(LOG_TAG, "Se llamo a messageSent");
        SMSCore smscore = new SMSCore();
        // SendSMS2 necesita como  tercer parametro un context
        // Pero no se que contexto pasarle
        // Probe con:  getBaseContext(), MainActivity.this y getApplicationContext();
        // Pero en el MessageReceiver no me llega nada
        smscore.SendSMS2("10001", "501", this);


//        String SENT = "sms_sent";
//        String DELIVERED = "sms_delivered";
//        Log.d(LOG_TAG, "Se llamo a message Sent");
//        String phone=to.getText().toString().trim();
//        Log.d(LOG_TAG, phone);
//        String SmsText=msgInput.getText().toString().trim();
//        Log.d(LOG_TAG, SmsText);
//
//
//        PendingIntent sentPI = PendingIntent.getActivity(getApplicationContext(), 0, new Intent(SENT), 0);
//        PendingIntent deliveredPI = PendingIntent.getActivity(getApplicationContext(), 0, new Intent(DELIVERED), 0);
//
//        if(!phone.equals("") && !SmsText.equals(""))
//        {
//            SmsManager smsManager = SmsManager.getDefault();
//            smsManager.sendTextMessage(phone, null,SmsText, null, null);
//            String phone2 = GetPhoneNumberFromSMSText();
//            Log.d(LOG_TAG, phone2);
//            Toast.makeText(getApplicationContext(),"sent",Toast.LENGTH_SHORT).show();
//
//        }
//        else {
//
//            Toast.makeText(getApplicationContext(),"fill the fields",Toast.LENGTH_SHORT).show();
//        }

    }





    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode==100 && grantResults[0]==PackageManager.PERMISSION_GRANTED && grantResults[1]==PackageManager.PERMISSION_GRANTED && grantResults[2]==PackageManager.PERMISSION_GRANTED)
        {
            MessageSent();
        }
        else {
            Toast.makeText(getApplicationContext(),"permission denied",Toast.LENGTH_LONG).show();
        }
    }

}
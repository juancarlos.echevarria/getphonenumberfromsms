package com.example.sendmessage;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsMessage;
import android.util.Log;
import android.provider.Telephony;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.ArrayList;
import java.util.List;
import android.os.Bundle;


public class MessageReceiver extends  BroadcastReceiver{

    public static final String LOG_TAG = "///MY LOG.";

    final String GetNumberAddress="10001";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(LOG_TAG, "Called onReceive");
        if(intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")){
            Bundle bundle = intent.getExtras();
            Object[] pdus = (Object[]) bundle.get("pdus");
            SmsMessage[] messages = new SmsMessage[pdus.length];
            StringBuilder sb = new StringBuilder();
            String address = "";
            String message = "";

            for(int i = 0; i < messages.length; i++){
                messages[i]=SmsMessage.createFromPdu((byte[])pdus[i]);
                sb.append("SMS RECIBIDO:\n");
                address=messages[i].getDisplayOriginatingAddress();
                message=messages[i].getDisplayOriginatingAddress();
                sb.append(address+"\n");
                sb.append("contenido:"+messages[i].getDisplayMessageBody());
            }

            SMSCore.PhoneNumber = SMSCore.GetPhoneNumberFromSMSText(sb.toString());
            Log.d(LOG_TAG, "Phone number is: " + SMSCore.PhoneNumber);
        }



    }

}
